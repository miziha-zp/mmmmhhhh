import numpy as np
import pandas as pd 
from datetime import datetime, timedelta
import gc
import lightgbm as lgb
from sklearn.model_selection import GroupKFold, StratifiedKFold, train_test_split

def get_lgbm_varimp(model, train_columns, max_vars=500000000):
    
    if "basic.Booster" in str(model.__class__):
        # lightgbm.basic.Booster was trained directly, so using feature_importance() function 
        cv_varimp_df = pd.DataFrame([train_columns, model.feature_importance()]).T
    else:
        # Scikit-learn API LGBMClassifier or LGBMRegressor was fitted, 
        # so using feature_importances_ property
        cv_varimp_df = pd.DataFrame([train_columns, model.feature_importances_]).T

    cv_varimp_df.columns = ['feature_name', 'varimp']

    cv_varimp_df.sort_values(by='varimp', ascending=False, inplace=True)

    cv_varimp_df = cv_varimp_df.iloc[0:max_vars]   

    return cv_varimp_df

def train_kFold_lgb_ranking(lgb_valid_data, lgb_test_data, featureslist, categorical_feature, groupname='customer_id', label='label', Kfold=3, num_boost_round=500):
    '''
    train lgb with lambda rank loss.
    return valid_out, test_out
    '''
#     zero_features, feature_importances = identify_zero_importance_features(lgb_valid_data[featureslist], lgb_valid_data['score'], 2)
#     print(zero_features)
#     print(feature_importances)
#     featureslist = [fea for fea in featureslist if fea not in zero_features]

    lgb_valid_data[groupname] = lgb_valid_data[groupname].astype('category')
    lgb_test_data[groupname] = lgb_test_data[groupname].astype('category')

    # print(featureslist)
    params = {
        'boosting_type': 'gbdt',
        'objective' : 'lambdarank',# rank_xendcg, lambdarank # 756
        # 'objective' : 'binary',
        'metric': ['ndcg'],
        "eval_at":[10],
        'num_leaves':64,
        'lambda_l1': 0.1,
        'lambda_l2': 0.5,
        # 'max_depth': -1,
        'learning_rate': 0.01,
        # 'min_child_samples':10,
        'feature_fraction': 0.9,
        'bagging_fraction': 0.9,
        'random_state':42,
        'num_threads':20
    }
    kFOLD_NUM = Kfold
    group_kfold = GroupKFold(n_splits=kFOLD_NUM)   
    groups = lgb_valid_data[groupname]
    X, y = lgb_valid_data[featureslist], lgb_valid_data[label]
    print(featureslist)
    test = lgb_test_data
    train_temp = lgb_valid_data[['customer_id', 'article_id']]
    valid_temp = lgb_test_data[['customer_id', 'article_id']]

    oof_prob = np.zeros(len(X))
    preds = np.zeros(len(test)) 
    print('--8'*100)
    fold = 1
    for train_index, test_index in group_kfold.split(X, y, groups):
        # print("TRAIN:", train_index, "TEST:", test_index)
#         X_train, X_valid = X.iloc[train_index], X.iloc[test_index]
#         y_train, y_valid = y.iloc[train_index], y.iloc[test_index]

        X_train, X_valid = X.iloc[train_index], lgb_test_data[featureslist]
        y_train, y_valid = y.iloc[train_index], lgb_test_data[label]
        
        # print(X_train.columns)

        fold_temp = train_temp.iloc[train_index]
        fold_temp['group'] = fold_temp.groupby('customer_id')['customer_id'].transform('size')
        temp = fold_temp[['customer_id','group']].drop_duplicates()
        train_group = temp['group'].tolist()
        print(sum(train_group))

        fold_temp = valid_temp
        fold_temp['group'] = fold_temp.groupby('customer_id')['customer_id'].transform('size')
        temp = fold_temp[['customer_id','group']].drop_duplicates()
        valid_group = temp['group'].tolist()
        print(sum(valid_group))

        dtrain = lgb.Dataset(X_train, label=y_train, group=train_group)
        dvalid = lgb.Dataset(X_valid, label=y_valid, group=valid_group)
        
        lgb_model = lgb.train(
                params,
                dtrain,
                num_boost_round=num_boost_round,
                valid_sets=[dtrain, dvalid],
                early_stopping_rounds=50,
                verbose_eval=50,
                categorical_feature=categorical_feature,
            )
        
        feature_columns = featureslist 
        print()
        imp = get_lgbm_varimp(lgb_model, featureslist, 100000000)
#         oof_prob[test_index] = lgb_model.predict(X_valid)
        lgb_model.save_model('model/modelFold'+str(fold)+'.txt')
        fold = fold + 1
        preds += lgb_model.predict(test[featureslist]) / Kfold
        print(imp.head(30))
        # print(imp.tail(30)) 
        
    from sklearn.metrics import roc_auc_score
    print('AUCCCCC:', roc_auc_score(lgb_test_data[label], preds))
    return roc_auc_score(lgb_test_data[label], preds)
#     return preds
    